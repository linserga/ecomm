<?php

class ProductsController extends \BaseController {

	
	public function __construct(){
		parent::__construct();
		$this->beforeFilter('csrf', ['on' => 'post']);
		$this->beforeFilter('admin');
	}
	
	public function index()
	{

		$categories = [];
		foreach(Category::all() as $category){
			$categories[$category->id] = $category->name;
		}
		return View::make('products.index')		
		->withProducts(Product::all())
		->withCategories($categories);
		
	}

	public function create(){

		
	}

	public function store()
	{
		$validation = Validator::make(Input::all(), Product::$rules);
		$image = Input::file('image');
		if($image){
			$filename = date('Y-m-d-H:i:s').'-'.$image->getClientOriginalName();
			Image::make($image->getRealPath())->resize(468, 249)->save('public/img/products/'.$filename);
		}
		
		if($validation->passes()){
			Product::create([
				'category_id' => Input::get('category_id'),
				'title' => Input::get('title'),
				'description' => Input::get('description'),
				'price' => Input::get('price'),
				'image' => 'img/products/'.$filename
				]);

			return Redirect::back()->withMessage('Product was successfully created');
		}else{
			return Redirect::back()->withErrors($validation)->withInput();
		}
	}

	
	public function show($id)
	{
		//
	}

	
	public function edit($id)
	{
		//
	}

	
	public function update($id)
	{
		$product = Product::find($id);

		if($product){
			$product->availability = Input::get('availability');
			$product->save();

			return Redirect::action('ProductsController@index')
			->withMessage('Product was successfully updated');
		}else{
			return Redirect::back()->withMessage('Invalid Product');
		}

	}

	
	public function destroy($id)
	{
		$product = Product::find($id);

		if($product){
			File::delete('public/'.$product->image);
			$product->delete();
			return Redirect::back()->withMessage('Product was successfully deleted');
		}else{
			return Redirect::back()->withMessage('Something went wrong, please try aganin');
		}
	}

}