<?php

class UsersController extends \BaseController {

	
	public function create()
	{
		return View::make('users.create');
	}

	public function store()
	{
		$validation = Validator::make(Input::all(), User::$rules);

		if($validation->passes()){
			User::create([
				'firstname' => Input::get('firstname'),
				'lastname' => Input::get('lastname'),
				'email' => Input::get('email'),
				'password' => Hash::make(Input::get('password'))
				]);

			return Redirect::action('SessionsController@create')
			->withMessage('Thank you for creating new account. Please log in');
		}else{
			return Redirect::back()
			->withErrors($validation)
			->withInput()			
			->withMessage('Something went wrong');
		}
	}
}