<?php

class CategoriesController extends \BaseController {

	
	public function __construct(){

		parent::__construct();

		$this->beforeFilter('csrf', ['on' => 'post']);
		$this->beforeFilter('admin');
	}
	/**
	 * Display a listing of the resource.
	 * GET /categories
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('categories.index')		
		->withCategories(Category::all());
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /categories/create
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /categories
	 *
	 * @return Response
	 */
	public function store()
	{
		$validation = Validator::make(Input::all(), Category::$rules);

		if($validation->passes()){
			Category::create([
				'name' => Input::get('name')
				]);

			return Redirect::back()->withMessage('Category was successfully created');
		}else{
			return Redirect::back()->withErrors($validation)->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 * GET /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('categories.show')
		->withProducts(Product::where('category_id', $id)->paginate(6))
		->withCategory(Category::find($id));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /categories/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$category = Category::find($id);

		if($category){
			$category->delete();
			return Redirect::back()->withMessage('Category was successfully deleted');
		}else{
			return Redirect::back()->withMessage('Something went wrong, please try aganin');
		}
	}

}