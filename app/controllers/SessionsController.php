<?php

class SessionsController extends \BaseController {

	
	public function create()
	{
		return View::make('sessions.create');
	}

	public function store()
	{
		if(Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')])){
			return Redirect::intended()->withMessage('Thanks for sign in');
		}else{
			return Redirect::back()->withMessage('Wrong credentials');
		}
	}

	public function destroy()
	{
		Auth::logout();
		return Redirect::to('login')->withMessage('You have been logged out');
	}

}