<?php

class StoresController extends \BaseController {

	
	public function __construct(){
		parent::__construct();
		$this->beforeFilter('csrf', ['on' => 'post']);
	}
	
	public function index()
	{
		return View::make('stores.index')
		->withProducts(Product::take(4)->orderBy('created_at', 'DESC')->get());
	}
	
	public function show($id)
	{
		return View::make('stores.show')
		->withProduct(Product::find($id));
	}

	public function search(){
		$keyword = Input::get('keyword');
		
		return View::make('stores.search')
		->withProducts(Product::where('title', 'LIKE', '%'.$keyword.'%')->get())
		->withKeyword($keyword);
	}

	public function contact(){
		return View::make('stores.contact');
	}
}