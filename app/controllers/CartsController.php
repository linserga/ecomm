<?php

class CartsController extends \BaseController {

	
	public function __construct(){
		parent::__construct();
		$this->beforeFilter('auth');
	}
	/**
	 * Display a listing of the resource.
	 * GET /carts
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('carts.index')
		->withProducts(Cart::contents());
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /carts/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /carts
	 *
	 * @return Response
	 */
	public function store()
	{
		$product = Product::find(Input::get('id'));
		$quantity = Input::get('quantity');

		Cart::insert([
			'id' => $product->id,
			'name' => $product->title,
			'price' => $product->price,
			'quantity' => $quantity,
			'image' => $product->image
			]);

		return Redirect::action('CartsController@index');
	}

	/**
	 * Display the specified resource.
	 * GET /carts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /carts/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /carts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /carts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$id = Input::get('id');
		//Cart::destroy();
		$item = Cart::item($id);		
		$item->remove();

		return Redirect::action('CartsController@index');
	}

}