<?php

Route::get('/', 'StoresController@index');

Route::get('login', 'SessionsController@create');

Route::get('logout', 'SessionsController@destroy');

Route::get('signup', 'UsersController@create');

Route::get('search', 'StoresController@search');

Route::get('category/{id}', 'CategoriesController@show');

Route::get('deleteItem/{id}', 'CartsController@destroy');

Route::get('contact', 'StoresController@contact');

Route::resource('admin/categories', 'CategoriesController');

Route::resource('admin/products', 'ProductsController');

Route::resource('store', 'StoresController');

Route::resource('session', 'SessionsController');

Route::resource('users', 'UsersController', ['only' => 'store']);

Route::resource('cart', 'CartsController');
