@extends('layouts.main')
@section('content')
	<div id='admin'>
		<h1>Products Admin Panel</h1>
		<h2>Products</h2>
		<ul>
			@foreach($products as $product)
				<li>
					{{HTML::image($product->image, $product->title, ['width' => '50'])}}
					{{$product->title}} -
					{{Form::open(['action' => ['ProductsController@destroy', $product->id], 'method'=> 'delete', 'class' => 'form-inline'])}}
						{{Form::submit('Delete')}} -
					{{Form::close()}}
					{{Form::open(['action'=> ['ProductsController@update', $product->id], 'method' => 'put', 'class' => 'form-inline'])}}
						{{Form::select('availability', ['1' => 'In Stock', '0' => 'Out of Stock'], $product->availability)}}
						{{Form::submit('Update')}}
					{{Form::close()}}
				</li>
			@endforeach
		</ul>

		<h2>Create New Product</h2>
		@include('_partials.errors')
		{{Form::open(['action' => 'ProductsController@store', 'files' => true])}}
			<div>
				{{Form::label('category_id', 'Category')}}
				{{Form::select('category_id', $categories)}}
			</div>
			<div>
				{{Form::label('title')}}
				{{Form::text('title')}}
			</div>
			<div>
				{{Form::label('description')}}
				{{Form::textarea('description')}}
			</div>
			<div>
				{{Form::label('price')}}
				{{Form::text('price', null, ['class' => 'form-price'])}}
			</div>
			<div>
				{{Form::label('image', 'Choose an image')}}
				{{Form::file('image')}}
			</div>
			<br>
				{{Form::submit('Create Product', ['class' => 'btn btn-success'])}}

		{{Form::close()}}
	</div><!--end admin-->
@stop