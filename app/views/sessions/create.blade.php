@extends('layouts.main')
@section('content')
	<div id='admin'>
		<h2>Login</h2>
		{{Form::open(['action' => 'SessionsController@store'])}}
			
			{{Form::label('email')}}
			{{Form::email('email')}}

			{{Form::label('password')}}
			{{Form::password('password')}}

			<br><br>
			{{Form::submit('Login', ['class' => 'btn btn-success'])}}
		{{Form::close()}}
	</div>
@stop