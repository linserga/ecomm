@extends('layouts.main')
@section('content')
<div id='admin'>
	{{Form::open(['action' => 'UsersController@store'])}}
		@include('_partials.errors')

		{{Form::label('firstname')}}
		{{Form::text('firstname')}}

		{{Form::label('lastname')}}
		{{Form::text('lastname')}}

		{{Form::label('email')}}
		{{Form::email('email')}}

		{{Form::label('password')}}
		{{Form::password('password')}}

		{{Form::label('password_confirmation', 'Confirm')}}
		{{Form::password('password_confirmation')}}

		<br><br>
		{{Form::submit('Sign up', ['class' => 'btn btn-success'])}}

	{{Form::close()}}
</div>
@stop
