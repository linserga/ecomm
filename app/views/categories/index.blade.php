@extends('layouts.main')
@section('content')
	<div id='admin'>
		<h2>Categories Admin Panel</h2>
		<ul>
			@foreach($categories as $category)
				<li>
					{{$category->name}} -
					{{Form::open(['action' => ['CategoriesController@destroy', $category->id], 'method' => 'delete', 'class' => 'form-inline'])}}
						{{Form::submit('Delete')}}
					{{Form::close()}}
				</li>
			@endforeach
		</ul>
	<h2>Create Category</h2>
	@include('_partials.errors')
	{{Form::open(['action' => 'CategoriesController@store'])}}
		{{Form::label('name')}}
		{{Form::text('name')}}
		{{Form::submit('Create Category', ['class' => 'btn btn-success btn-xs'])}}
	{{Form::close()}}
	</div>
@stop
