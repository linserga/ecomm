@extends('layouts.main')
@section('content')
<div id="product-image">
    {{HTML::image($product->image, $product->title)}}
</div><!-- end product-image -->
<div id="product-details">
    <h1>{{$product->title}}</h1>
    <p>{{$product->description}}</p>

    <hr />

    <p>
        {{Form::open(['action' => 'CartsController@store'])}}
            {{Form::hidden('quantity', 1)}}
            {{Form::hidden('id', $product->id)}}
            <button type='submit' class='cart-btn'>
                <span class='price'>{{$product->price}}</span>
                {{HTML::image('img/white-cart.gif', 'Add to Cart')}}
                ADD TO CART
            </button>
        {{Form::close()}}
        </p>
</div><!-- end product-details -->
<div id="product-info">
    <p class="price">{{$product->price}}</p>
    <h5>Availability: <span class="{{Availability::displayClass($product->availability)}}">{{Availability::display($product->availability)}}</span></h5>
    <p>Product Code: <span>{{$product->id}}</span></p>
</div><!-- end product-info -->
@stop
